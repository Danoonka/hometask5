import { University } from "./university/university";
import { Student } from "./university/students";
import { Courses, Departments, Point } from "./university/enums";
import { Teacher } from "./university/teacher";
import { Department } from "./university/departments";
import { Course } from "./university/course";
import { PointClass } from "./university/point";
const universityCoordinates = {
    latitude: 40.7128,
    longitude: -74.0060,
};
const university = new University('Open University', universityCoordinates);
const mathCourse = new Course(1, Courses.mathCourse, 3);
const physicsCourse = new Course(2, Courses.physicsCourse, 4);
const point = new PointClass(1, Courses.mathCourse, Point.A);
const dep = new Department(1, Departments.Mathematics, [mathCourse, physicsCourse]);
const student = new Student(1, 'Kate', 'Shevchenko', 20, dep, [mathCourse, physicsCourse], [point]);
const teacher = new Teacher(1, 'Larysa', 'Dyka', 45, dep, [mathCourse]);
university.addStudent(student);
university.addTeacher(teacher);
const students = university.getStudents();
const teachers = university.getTeachers();
console.log('Students:');
students.forEach((student) => {
    console.log(`- ${student.firstName} ${student.lastName}`);
});
console.log('Teachers:');
teachers.forEach((teacher) => {
    console.log(`- ${teacher.firstName} ${teacher.lastName}`);
});
