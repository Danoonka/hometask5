import {Coordinates} from "./types";
import {UniversityDepartment, UniversityStudent, UniversityTeacher} from "./interfaces";

class University {
    private name: string;
    private location: Coordinates;
    private readonly students: UniversityStudent[];
    private readonly teachers: UniversityTeacher[];
    private readonly departments: UniversityDepartment[];

    constructor(name: string, location: Coordinates) {
        this.name = name;
        this.location = location;
        this.students = [];
        this.teachers = [];
        this.departments = []
    }

    addStudent(student: UniversityStudent): void {
        this.students.push(student);
    }

    addTeacher(teacher: UniversityTeacher): void {
        this.teachers.push(teacher);
    }

    addDepartment(department: UniversityDepartment): void {
        this.departments.push(department);
    }

    removeStudent(student: UniversityStudent): void {
        const index = this.students.indexOf(student);
        if (index !== -1) {
            this.students.splice(index, 1);
        }
    }

    removeTeacher(teacher: UniversityTeacher): void {
        const index = this.teachers.indexOf(teacher);
        if (index !== -1) {
            this.teachers.splice(index, 1);
        }
    }

    removeDepartment(department: UniversityDepartment): void {
        const index = this.departments.indexOf(department);
        if (index !== -1) {
            this.departments.splice(index, 1);
        }
    }
    getStudents(): UniversityStudent[] {
        return this.students;
    }

    getTeachers(): UniversityTeacher[] {
        return this.teachers;
    }

    getDepartment():UniversityDepartment[]{
        return this.departments
    }
}

export {University};
