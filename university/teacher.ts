import {UniversityPerson} from "./universityPerson";
import {CourseItem, UniversityTeacher} from "./interfaces";
import {Department} from "./departments";
import {ReadonlyID} from "./types";

export class Teacher extends UniversityPerson implements UniversityTeacher {
    private _universityDepartment: Department;
    private _courseList: CourseItem[];

    constructor(
        id: ReadonlyID,
        firstName: string,
        lastName: string,
        age: number,
        universityDepartment: Department,
        courseList: CourseItem[]
    ) {
        super(id, firstName, lastName, age);
        this._universityDepartment = universityDepartment;
        this._courseList = courseList;
    }

    get universityDepartment(): Department {
        return this._universityDepartment;
    }

    set universityDepartment(department: Department) {
        this._universityDepartment = department;
    }

    get courseList(): CourseItem[] {
        return this._courseList;
    }

    set courseList(courseList: CourseItem[]) {
        this._courseList = courseList;
    }
}