import {UniversityPerson} from "./universityPerson";
import {CourseItem, PointItem, UniversityStudent} from "./interfaces";
import {Department} from "./departments";
import {ReadonlyID} from "./types";

export class Student extends UniversityPerson implements UniversityStudent {
    private _universityDepartment: Department;
    private _courseList: CourseItem[];
    private _pointList: PointItem[];

    constructor(
        id: ReadonlyID,
        firstName: string,
        lastName: string,
        age: number,
        universityDepartment: Department,
        courseList: CourseItem[],
        pointList: PointItem[]
    ) {
        super(id, firstName, lastName, age);
        this._universityDepartment = universityDepartment;
        this._courseList = courseList;
        this._pointList = pointList;
    }

    get universityDepartment(): Department {
        return this._universityDepartment;
    }

    set universityDepartment(department: Department) {
        this._universityDepartment = department;
    }

    get courseList(): CourseItem[] {
        return this._courseList;
    }

    set courseList(courseList: CourseItem[]) {
        this._courseList = courseList;
    }

    get pointList(): PointItem[] {
        return this._pointList;
    }

    set pointList(pointList: PointItem[]) {
        this._pointList = pointList;
    }
}
