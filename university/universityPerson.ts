import {ReadonlyID} from "./types";

export class UniversityPerson {
    private readonly _id: ReadonlyID;
    private _firstName: string;
    private _lastName: string;
    private _age: number;

    constructor(id: ReadonlyID, firstName: string, lastName: string, age: number) {
        this._id = id;
        this._firstName = firstName;
        this._lastName = lastName;
        this._age = age;
    }

    get id(): ReadonlyID {
        return this._id;
    }

    get firstName(): string {
        return this._firstName;
    }

    set firstName(firstName: string) {
        this._firstName = firstName;
    }

    get lastName(): string {
        return this._lastName;
    }

    set lastName(lastName: string) {
        this._lastName = lastName;
    }

    get age(): number {
        return this._age;
    }

    set age(age: number) {
        this._age = age;
    }
}