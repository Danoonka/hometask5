import {CourseItem, PointItem} from "./interfaces";
import {Courses, Point} from "./enums";
import {ReadonlyID} from "./types";

export class PointClass implements PointItem {
    id: ReadonlyID
    courseItem: Courses;
    pointItem: Point;

    constructor(id: ReadonlyID, courseItem: Courses, pointItem: Point) {
        this.id = id;
        this.courseItem = courseItem;
        this.pointItem = pointItem;
    }

    get getId(): ReadonlyID {
        return this.id;
    }

    getPoint(): Point {
        return this.pointItem;
    }

    setPoint(point: Point) {
        this.pointItem = point;
    }

    getCourseItem(): Courses {
        return this.courseItem;
    }

    setCourseItem(courseItem: Courses) {
        this.courseItem = courseItem;
    }
}