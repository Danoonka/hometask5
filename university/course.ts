import {CourseItem} from "./interfaces";
import {ReadonlyID} from "./types";

export class Course implements CourseItem {
    id: ReadonlyID;
    name: string;
    credits: number;

    constructor(id: ReadonlyID, name: string, credits: number) {
        this.id = id;
        this.name = name;
        this.credits = credits;
    }

    get getId(): ReadonlyID {
        return this.id;
    }

    get getName(): string {
        return this.name;
    }

    set setName(name: string) {
        this.name = name;
    }

    getCredits(): number {
        return this.credits;
    }

    setCredits(credits: number) {
        this.credits = credits;
    }
}