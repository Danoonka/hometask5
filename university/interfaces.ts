import {Department} from "./departments"
import {Courses, Point} from "./enums";
import {ReadonlyID} from "./types";

export interface UniversityPerson {
    id: ReadonlyID;
    firstName: string;
    lastName: string;
    age: number;
}

export interface UniversityStudent extends UniversityPerson {
    universityDepartment: Department;
    courseList: CourseItem[];
    pointList: PointItem[];
}

export interface UniversityTeacher extends UniversityPerson {
    universityDepartment: Department;
    courseList: CourseItem[];
}

export type UniversityMember = UniversityStudent | UniversityTeacher;

export interface UniversityDepartment {
    id: ReadonlyID;
    name: string;
    courseList: CourseItem[];
}

export interface CourseItem {
    id: ReadonlyID;
    name: string;
    credits: number;
}

export interface PointItem {
    id: ReadonlyID
    courseItem: Courses;
    pointItem: Point;
}
