export type Coordinates = {
    latitude: number;
    longitude: number;
};


export type ReadonlyID = Readonly<number>;
