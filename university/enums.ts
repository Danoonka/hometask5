export enum Departments {
    ComputerScience = "Computer Science",
    Mathematics = "Mathematics",
    Physics = "Physics",
}

export enum Courses {
    mathCourse = "Mathematics",
    physicsCourse = "Physics",
}

export enum Point {
    A = "95-100",
    B = "85-95",
    C = "75-85",
    D = "65-75",
    E = "60-65"
}