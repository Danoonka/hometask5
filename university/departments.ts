import {CourseItem, UniversityDepartment} from "./interfaces";
import {Departments} from "./enums";
import {ReadonlyID} from "./types";

export class Department implements UniversityDepartment {
    id: ReadonlyID;
    name: Departments;
    courseList: CourseItem[];

    constructor(id: ReadonlyID, name: Departments, courseList: CourseItem[]) {
        this.id = id;
        this.name = name;
        this.courseList = courseList;
    }

    get getId(): ReadonlyID {
        return this.id;
    }

    get getName(): Departments {
        return this.name;
    }

    set setName(name: Departments) {
        this.name = name;
    }

    get getCourseList(): CourseItem[] {
        return this.courseList;
    }

    set setCourseList(courseList: CourseItem[]) {
        this.courseList = courseList;
    }
}